package co.mz.dmi.aulaspring.service;

import co.mz.dmi.aulaspring.model.Product;
import co.mz.dmi.aulaspring.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {


    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional(readOnly = true)
    public List<Product> findAll(){

        return productRepository.findAll();
    }

    public Product findById(Long id){

        return checkProduct(id);
    }

    @Transactional
    public Product save(Product product){
        return productRepository.save(product);
    }

    @Transactional
    public Product update(Long id, Product product){
        Product product1 = checkProduct(id);

       product1.setName(product.getName());
       product1.setPrice(product.getPrice());
       product1.setExpiresAt(product.getExpiresAt());

       productRepository.save(product1);
       return product;
    }

    public void delete(Long id){
        Product product1 = checkProduct(id);

        productRepository.delete(product1);
    }

    private Product checkProduct(Long id){

        return productRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("O producto com o id " + id + " nao existe"));
    }
}
