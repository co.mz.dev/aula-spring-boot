package co.mz.dmi.aulaspring.config;

import co.mz.dmi.aulaspring.model.Product;
import co.mz.dmi.aulaspring.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class ProductSeed implements CommandLineRunner {

    private final ProductRepository repository;

    public ProductSeed(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (repository.count() > 0)
                  return;

        System.out.println("Lista de productos vazia");
        Product product1 = new Product(null,"Bolacha",25.00, LocalDate.now().plusDays(4), 20 );
        Product product2 = new Product(null,"Farinha",55.00, LocalDate.now().plusMonths(1), 20 );

//        repository.save(product1);
//        repository.save(product2);
//
        repository.saveAll(List.of(product1, product2));

    }
}
